package mongo

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"

	"github.com/globalsign/mgo"
)

// docBuffer is an object that wraps a standard bytes.Buffer and extends it
// with two channels for synchronisation. It's meant to be used as an
// io.WriteCloser.
type docBuffer struct {
	closed  chan bool
	written chan error

	*bytes.Buffer
}

// newDocBuffer initializes a new docBuffer and returns the reference.
func newDocBuffer() *docBuffer {
	return &docBuffer{
		make(chan bool, 1),
		make(chan error, 1),
		bytes.NewBuffer(nil),
	}
}

// Close is used to implement synchronization for the buffer. Since our ultimate
// goal is to persist data to storage we need to write it at some point. This
// can't be done directly with the collection storage. Instead we use the closed
// channel to signal that the user has completed writing data to the buffer.
// After signaling that the write is completed we block until the function until
// the user signals that the data write has succeeded or failed over the written
// channel.
func (db *docBuffer) Close() (err error) {
	if db.Len() <= maxDocSize {
		db.closed <- true
		// This blocks the close call until the data is flushed to disk. We
		// could optionally skip this mechanic but that would introduce a loose
		// write logic where we flag data as written when it's actually still in
		// a buffer.
		err = <-db.written
		if err != nil {
			err = fmt.Errorf("Failed to write data -> %s", err)
		}
		return
	}
	db.closed <- false
	return errMaxSize
}

// IterReader implements an io.Reader interface for mgo.Iter
type IterReader struct {
	docRead bool
	doc     []byte
	off     int

	*mgo.Iter
}

// NewIterReader creates a new *IterReader for the provided mgo.Iter reference.
func NewIterReader(i *mgo.Iter) *IterReader {
	var r = IterReader{Iter: i}
	return &r
}

// updateState checks if the entire row has been read. If yes we flag docRead
// true indicating that in the next read cycle we should go to the next row.
// if we haven't finished reading the row we update the pointer telling us how
// far we have gotten.
func (ir *IterReader) updateState(n int) {
	if (n + ir.off) == len(ir.doc) {
		ir.docRead = true
	} else {
		ir.off += n
	}
}

// resetState resets the all state trackers.
func (ir *IterReader) resetState() {
	ir.docRead = false
	ir.doc = nil
	ir.off = 0
}

// Read data from the mgo.Iter into the byte slice provided.
func (ir *IterReader) Read(b []byte) (n int, err error) {
	var nl int
	if len(b) == 0 {
		ir.Close()
		return 0, nil
	}
	if ir.docRead {
		nl = copy(b, []byte("\n"))
		if ir.Done() {
			ir.Close()
			return nl, io.EOF
		}
		ir.resetState()
	}
	if ir.doc == nil {
		var row interface{}
		ir.Next(&row)
		ir.doc, err = json.Marshal(row)
	}
	n = copy(b[nl:], ir.doc[ir.off:])
	ir.updateState(n)
	n = n + nl
	return
}
