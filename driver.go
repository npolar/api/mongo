// Package mongo is a reference storage implementation for the
// [npolar/api](https://gitlab.com/npolar/api) project based on MongoDB. The
// [globalsign/mgo](https://godoc.org/github.com/globalsign/mgo) package is used
// to communicate with MongoDB. Document storage and internal metadata are
// stored in collections. File storage uses GridFS.
package mongo

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"mime"
	"strings"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"gitlab.com/npolar/api/core"
)

const (
	driverName = "mongo"
	anyType    = "*/*"
	maxDocSize = 16777216 // MongoDB docs can't be larger than 16MB
)

var (
	errMaxSize = fmt.Errorf("max size exceeded: %d bytes allowed", maxDocSize)
)

// Driver is a placeholder struct used to implement the driver logic defined in
// the [npolar/api/core](https://gitlab.com/npolar/api/core) package.
type Driver struct{}

// Open a new MongoDB connection and do initial configuration based on the
// StorageType and opts provided.
func (m Driver) Open(st core.StorageType, opts json.RawMessage) (core.Conn, error) {
	// Initialize a default connection
	var c = Conn{
		Servers:     []string{"localhost:27017"}, // MongoDB default localhost addr
		storageType: st,
	}

	// Decode the opts into the connection struct for further configuration
	err := json.Unmarshal(opts, &c)

	// If no decoding error occurred we continue connection setup.
	if err == nil {
		// Connect to the MongoDB instance(s) and configure db/collection access
		if c.Session, err = mgo.Dial(c.serverList()); err == nil {
			// Switch the connection into a safer operation mode.
			c.SetSafe(&mgo.Safe{
				WMode: "majority",
				RMode: "majority",
				FSync: true,
			})

			// Check if an authentication database was provided in the options.
			// If not we default to the storage database.
			if adb := c.AuthDB; adb == "" {
				c.AuthDB = c.Database
			}

			// Login to the database
			if err = c.DB(c.AuthDB).Login(c.User, c.Pass); err != nil {
				return nil, fmt.Errorf("Login: %s", err)
			}

			// Initialize storage based on the configured StorageType
			switch st {
			case core.DocStorage:
				if c.Collection == "" {
					c.Close()
					return nil, fmt.Errorf("no docCollection configured")
				}
				c.doc = c.DB(c.Database).C(c.Collection)
				c.docInfo = c.DB(c.Database).C(fmt.Sprintf(
					"%sInfo", c.Collection,
				))
			case core.FileStorage:
				if c.Collection == "" {
					c.Close()
					return nil, fmt.Errorf("no fileCollection configured")
				}
				c.file = c.DB(c.Database).GridFS(c.Collection)
				c.fileInfo = c.DB(c.Database).C(fmt.Sprintf(
					"%sInfo", c.Collection,
				))
			}

			bson.SetJSONTagFallback(true)
		}
	}

	return c, err
}

// Conn wraps the MongoDB configuration and implements the core.Conn interface.
type Conn struct {
	Database       string   `json:"database"`
	Collection     string   `json:"collection"`
	SupportedTypes []string `json:"supportedTypes"`
	Servers        []string `json:"servers"`
	User           string   `json:"username"`
	Pass           string   `json:"password"`
	AuthDB         string   `json:"authDB"`

	storageType core.StorageType

	*mgo.Session
	doc      *mgo.Collection // Document collection
	docInfo  *mgo.Collection // Info collection for documents
	file     *mgo.GridFS     // GridFS prefix
	fileInfo *mgo.Collection // Info collection for files
}

// serverList returns a comma separated address list as required by mgo.
func (c Conn) serverList() string {
	return strings.Join(c.Servers, ",")
}

// supportedType checks if the argument is present in the supported list of
// Types for the connection.
func (c Conn) supportedType(t string) bool {
	var supported bool

	t, _, err := mime.ParseMediaType(t)
	if err != nil {
		log.Printf("Invalid media type -> \n\tInput: %s\n\tError: %s\n", t, err)
		return supported
	}

	for _, st := range c.SupportedTypes {
		if t == st || st == anyType {
			supported = true
			break
		}
	}

	return supported
}

// Close wraps the mgo.Session.Close() method and returns nil when called.
//	From the mgo docs: Close terminates the session. It's a runtime error to use
//	a session after it has been closed.
//
//	reference: https://godoc.org/github.com/globalsign/mgo#Session.Close
func (c Conn) Close() error {
	c.Session.Close()
	return nil
}

// Formats returns an string array containing the allowed formats for this
// connection. Formats should be specified in the media type format as defined
// in RFC6838: https://tools.ietf.org/html/rfc6838
func (c Conn) Formats() []string {
	return c.SupportedTypes
}

// Read retrieves an item from document of file storage using the provided Info.
// If an item is found it returns an io.ReadSeeker containing the result.
func (c Conn) Read(info *core.Info, ctx context.Context) (io.ReadSeeker, error) {
	var data []byte
	var err error

	// Fetch the owners from the request
	owner := info.Owner

	// Retrieve the full info from the system.
	if err = c.ReadInfo(info); err != nil {
		return nil, err
	}

	// Check if the item was deleted. If so we return the info as the result
	// and a deleted error.
	if info.IsDeleted() {
		if data, err = json.Marshal(info); err == nil {
			err = fmt.Errorf("deleted")
		}
		return bytes.NewReader(data), err
	}

	// Check if the items has been released. If not check the request owner
	// against the owners for the item.
	authorized := info.IsReleased()
	if !authorized {
		if len(owner) != 0 {
			for o := range owner {
				if rights, ok := info.Owner[o]; ok && rights.Read {
					authorized = ok
					break
				}
			}
		} else {
			// If no owners were present in the info object we return the info
			// object as json and an unauthorized error.
			if data, err = json.Marshal(info); err == nil {
				err = fmt.Errorf("unauthorized")
			}
			return bytes.NewReader(data), err
		}
	}

	// when the item isn't released and the provided user doesn't have read
	// access we return the info to the caller together with a forbidden error.
	if !authorized {
		if data, err = json.Marshal(info); err == nil {
			err = fmt.Errorf("forbidden")
		}
		return bytes.NewReader(data), err
	}

	// Switch to the correct read mode based on the storage type
	if c.storageType == core.DocStorage {
		return c.readDoc(info)
	}

	return c.readFile(info, ctx)
}

// readDoc retrieves a document from the database based on the Info object.
// @TODO add logic for restricted documents and user access.
func (c Conn) readDoc(info *core.Info) (io.ReadSeeker, error) {
	var doc interface{}
	var data []byte
	var err error

	agg := []bson.M{
		{"$match": bson.M{"_id": info.DocID}}, // Lookup ID
		{"$project": bson.M{"_id": 0}},        // Remove _id
	}

	if err = c.doc.Pipe(agg).AllowDiskUse().One(&doc); err == nil {
		data, err = json.Marshal(doc)
	}

	return bytes.NewReader(data), err
}

// readFile opens a GridFS file based on the Info object provided. It uses a
// cancelation context to close the file after reading concludes. If no context
// is provided an error will be returned.
func (c Conn) readFile(info *core.Info, ctx context.Context) (io.ReadSeeker, error) {
	if ctx == nil {
		return nil, fmt.Errorf("no cancellation context provided")
	}
	f, err := c.file.OpenId(info.ID)
	if err == nil {
		go func() {
			<-ctx.Done()
			if err = f.Close(); err != nil {
				log.Println("Error closing GridFS file ->", err)
			}
		}()
	}

	return f, err
}

// Create inserts a new item into a collection or GridFS prefix based on the
// storage mode in use. Create verifies which types are configured for storage.
// If the item being created isn't of a supported type it will get rejected
// with an unsupported type error.
func (c Conn) Create(info *core.Info, r io.Reader) error {
	// Check if the content being created is supported
	if !c.supportedType(info.Type) {
		return fmt.Errorf("unsupported media type")
	}

	// Check if an owner is present. If not we return an unauthorized error.
	if len(info.Owner) == 0 {
		return fmt.Errorf("unauthorized")
	}

	// Loop the owner list. If one of them has Create rights we flag authorized
	// as true and break out of the loop.
	var authorized bool
	for _, r := range info.Owner {
		if r.Create {
			authorized = true
			break
		}
	}

	// If not authorized to perform the action we return forbidden.
	if !authorized {
		return fmt.Errorf("forbidden")
	}

	// Switch to the correct storage mode
	if c.storageType == core.DocStorage {
		return c.createDoc(info, r)
	}
	return c.createFile(info, r)
}

// createDoc uses the info object to insert the document in the reader into
// MongoDB. If the write succeeds it updates the timestamps in the info object
// and calls WriteInfo to commit the medata to storage.
func (c Conn) createDoc(info *core.Info, r io.Reader) error {
	var n int64
	var err error
	var buf bytes.Buffer

	// Read the data into a buffer
	if n, err = buf.ReadFrom(r); err != nil {
		return err
	}

	// Check if the length is within the max document size for MongoDB (16MB).
	if n < maxDocSize {
		var d = make(map[string]interface{})

		// Attempt to read the document contents into the map. If that succeeds
		// we inject the correct document ID and insert the data.
		if err = json.Unmarshal(buf.Bytes(), &d); err == nil {
			d["_id"] = info.DocID
			err = c.doc.Insert(d)
		}

		// Update timestamps and write the metadata.
		if err == nil {
			if info.Name == "" {
				info.Name = fmt.Sprintf("%s.json", info.DocID)
			}
			info.Size = n
			info.SetCreatedNow()
			info.SetModifiedNow()
			info.SetReleasedNow()
			err = c.WriteInfo(info)
		}
	} else {
		err = errMaxSize
	}

	return err
}

// createFile creates a new GridFS file based on the info object and the content
// available on the io.Reader. It will also encode the info object as additional
// metadata in the Files collection.
func (c Conn) createFile(info *core.Info, r io.Reader) error {
	// Generate a new ID for the item
	info.ID = core.UUIDv4()

	// If the info object doesn't contain a FileID generate one
	if info.FileID == "" {
		info.FileID = core.UUIDv4()
	}

	// If no name is set use the FileID as the name.
	if info.Name == "" {
		info.Name = info.FileID
	}

	f, err := c.file.Create(info.Name)
	if err != nil {
		return err
	}

	info.Version.Increment(core.Major)
	// We'll track the content using our own ID.
	// @NOTE this method must be called before any data is written to the file.
	// If called after data is written it will only update the reference on the
	// last written chunk in GridFS leading to inaccessible data.
	f.SetId(info.ID)
	defer f.Close()

	// Copy the data over to the file
	var n int64
	n, err = io.Copy(f, r)

	// If no error during copying update and write the file metadata.
	if err == nil {
		info.Size = n
		info.SetCreatedNow()
		info.SetModifiedNow()
		info.SetReleasedNow()

		// Files should always have a unique info ID since updates don't occur
		// on actual objects in place.

		// Call methods on the Grid file to set additional metadata.
		f.SetContentType(info.Type)
		f.SetUploadDate(info.Modified)

		// Write the info
		err = c.WriteInfo(info)
	} else {
		// If an error occurs during data copy we call Abort() which runs
		// cleanup on any content already created in MongoDB.
		f.Abort()
	}

	return err
}

// Update writes the readers contents to the item referenced in the info. When
// updating a previously deleted item the delete time will be reset and content
// will be re-inserted in the namespace defined by the info object.
func (c Conn) Update(info *core.Info, r io.Reader) error {
	// Check if the content being created is supported
	if !c.supportedType(info.Type) {
		return fmt.Errorf("unsupported media type")
	}

	var authorized bool
	var owner = info.Owner

	// Store the name and type in the info object as update values.
	var nName = info.Name
	var nType = info.Type
	var nRelease = info.Released

	// Retrieve the metadata for the appropriate item
	err := c.ReadInfo(info)
	if err != nil {
		return err
	}

	// Check if the user making the request has update rights on the item.
	if len(owner) != 0 {
		for o := range owner {
			if rights, ok := info.Owner[o]; ok && rights.Update {
				authorized = ok
				break
			}
		}
	} else {
		// If no owner is present we return an unauthorized error.
		return fmt.Errorf("unauthorized")
	}

	// If the user doesn't have rights to the item return a forbidden error.
	if !authorized {
		return fmt.Errorf("forbidden")
	}

	// If the item was previously deleted re-activate it by zeroing the time
	// and call Create to re-insert content on the ID.
	if info.IsDeleted() {
		info.Deleted = time.Time{}
		if c.storageType == core.DocStorage {
			return c.createDoc(info, r)
		}
	}

	// Write the updated info values (if any) to the retrieved info object.
	if nName != "" {
		info.Name = nName
	}
	if nType != "" {
		info.Type = nType
	}
	if !nRelease.IsZero() {
		info.Released = nRelease
	}

	// Select a path based on the storage mode
	if c.storageType == core.DocStorage {
		return c.updateDoc(info, r)
	}
	return c.updateFile(info, r)
}

// updateDoc looks up the previous item and overwrites it with the data in the
// provided reader. The info is update to reflect the modification and written
// to the info collection
func (c Conn) updateDoc(info *core.Info, r io.Reader) error {
	var n int64
	var err error
	var buf bytes.Buffer
	n, err = buf.ReadFrom(r)
	if err != nil {
		return err
	}

	if n < maxDocSize {
		var d interface{}
		err = json.Unmarshal(buf.Bytes(), &d)

		// Write the update
		if err == nil {
			err = c.doc.UpdateId(info.DocID, d)
		}

		// Update and write metadata
		if err == nil {
			info.SetModifiedNow()
			err = c.WriteInfo(info)
		}
	} else {
		err = errMaxSize
	}

	return err
}

// updateFile creates a new file with the updated file content. In practice it's
// not recommended to do in place updates on GridFS (described in this section:
// https://docs.mongodb.com/manual/core/gridfs/index.html#when-to-use-gridfs).
// We will instead create a new entry and if that succeeds spawn a background
// worker that runs a delete on the old item.
//	@NOTE the cleanup routine runs async from the main thread. It will not keep
//	the program alive even though it is still performing work. In order	for
//	cleanup to be completed correctly you need to take into account the required
//	runtime for the background work. If not we might retain data and file info.
func (c Conn) updateFile(info *core.Info, r io.Reader) error {
	orig := *info
	err := c.createFile(info, r)
	// if no error was detected creating the new content we run a cleanup
	// routine for the old content.
	if err == nil {
		go func() {
			if derr := c.deleteFile(&orig); err == nil {
				if derr = c.deleteFileInfo(&orig); derr != nil {
					log.Println("File Update - Ref cleanup ->", derr)
				}
			} else {
				log.Println("File Update - Data cleanup ->", derr)
			}
		}()
	}
	return err
}

// Delete an item from the collection/GridFS prefix based on the storage mode.
// Info objects are retained after deletion. This ensure that we don't create
// any dead references. If data is gone the info should be returned.
func (c Conn) Delete(info *core.Info) error {
	var authorized bool
	var err error

	// Fetch the owners from the request
	owner := info.Owner

	// Retrieve the full info from the system.
	if err = c.ReadInfo(info); err != nil {
		return err
	}

	// Check if the item was deleted. If so we return the info as the result
	// and a deleted error.
	if info.IsDeleted() {
		return err
	}

	// If an owner is present check if he has the right to remove the data.
	if len(owner) != 0 {
		for o := range owner {
			if rights, ok := info.Owner[o]; ok && rights.Delete {
				authorized = ok
				break
			}
		}
	} else {
		// If no owner is present return unauthorized
		return fmt.Errorf("unauthorized")
	}

	// If the request owner doesn't have the correct access rights on the item
	// we return a forbidden error.
	if !authorized {
		return fmt.Errorf("forbidden")
	}

	if c.storageType == core.DocStorage {
		return c.deleteDoc(info)
	}
	return c.deleteFile(info)
}

// deleteDoc removes the document from the collection and flags it as deleted in
// the info collection. This prevents dead references for documents guarantueing
// reference persistence.
func (c Conn) deleteDoc(info *core.Info) error {
	err := c.doc.RemoveId(info.DocID)

	if err == nil {
		info.Deleted = time.Now().UTC()
		c.WriteInfo(info)
	}

	return err
}

// deleteFile removes the file refrenced in the info from GridFS. The info is
// updated to reflect the deletion and written to the info collection. this
// prevents dead references for files guarantueing reference persistence.
func (c Conn) deleteFile(info *core.Info) error {
	err := c.file.RemoveId(info.ID)

	if err == nil {
		info.Deleted = time.Now().UTC()
		c.WriteInfo(info)
	}

	return err
}

// NewWriteCloser returns a new io.WriteCloser based on the info object
// provided. This is convenient for use cases where you want to control the
// write actions directly (for example when creating archives on the fly).
//	- NewWriteCloser doesn't persist Info to disk so the caller needs to run a
//	Conn.WriteInfo(info) call to write metadata to disk.
//	- NewWriteCloser can't be used to update existing content.
func (c Conn) NewWriteCloser(info *core.Info) (io.WriteCloser, error) {
	// check if the type is supported
	if !c.supportedType(info.Type) {
		return nil, fmt.Errorf("unsupported media type")
	}

	// check if the info object was initialized with the proper basics
	if info.ID == "" {
		info.ID = core.UUIDv4()
	}

	if c.storageType == core.DocStorage {
		return c.docWriteCloser(info)
	}
	return c.fileWriteCloser(info)
}

// docWriteCloser uses docBuffer to expose an io.WriteCloser for documents. It
// uses a go routine to handle the write synchronization on Close().
func (c Conn) docWriteCloser(info *core.Info) (io.WriteCloser, error) {
	db := newDocBuffer()

	// Since this isn't a realtime writer we need to handle the flushing of data
	// to disk ourselves. When the client invokes the Close() method on our
	// document buffer we are going to call Create using the buffer as the
	// reader object.
	go func(i core.Info) {
		if <-db.closed {
			db.written <- c.createDoc(&i, db)
		}
	}(*info)

	return db, nil
}

// fileWriteCloser creates a new mgo.GridFile reference or an error and returns
// it to the caller. In addition it calls some basic metadata methods on the
// GridFile reference in order to set the proper name and ID in the database.
func (c Conn) fileWriteCloser(info *core.Info) (io.WriteCloser, error) {
	f, err := c.file.Create(info.Name)

	info.SetCreatedNow()

	if info.FileID == "" {
		info.FileID = core.UUIDv4()
	}

	if info.Name == "" {
		f.SetName(info.FileID)
	}

	f.SetContentType(info.Type)
	f.SetId(info.ID)
	return f, err
}

// WriteInfo will commit the info object to the designated info collection. if
// for some reason write should fail an error is returned to the caller.
func (c Conn) WriteInfo(info *core.Info) error {
	// Generate an info ID if not set.
	if info.ID == "" {
		info.ID = core.UUIDv4()
	}

	if info.DocID == "" {
		return fmt.Errorf("no docID provided")
	}

	if c.storageType == core.DocStorage {
		return c.writeDocInfo(info)
	}

	return c.writeFileInfo(info)
}

// writeDocInfo will insert a new metadata entry into the document info
// collection or update the existing one if found.
func (c Conn) writeDocInfo(info *core.Info) error {
	_, err := c.docInfo.Upsert(bson.M{"_id": info.ID}, info)
	return err
}

// writeFileInfo will insert a new metadata entry into the file info collection
// or update the existing one if found.
func (c Conn) writeFileInfo(info *core.Info) error {
	if info.FileID == "" {
		return fmt.Errorf("no FileID provided")
	}

	_, err := c.fileInfo.Upsert(bson.M{"_id": info.ID}, info)
	return err
}

// ReadInfo will retrieve the metadata object for specified item. If metadata is
// found the info reference will be updated with the retrieved information. If
// not an error will be returned.
func (c Conn) ReadInfo(info *core.Info) error {
	if c.storageType == core.DocStorage {
		return c.readDocInfo(info)
	}
	return c.readFileInfo(info)
}

// readDocInfo does a lookup based on the docID in the document info collection.
// If an item is found it will update the info reference. If not the lookup
// error is returned to the caller.
func (c Conn) readDocInfo(info *core.Info) error {
	var agg = []bson.M{
		{"$match": bson.M{
			"$or": []bson.M{
				{"docID": info.DocID},
				{"name": info.Name},
			}},
		},
	}
	return c.docInfo.Pipe(agg).AllowDiskUse().One(info)
}

// readFileInfo retrieves the info object for the matching file that was last
// modified. This is done because multiple info objects might exist for a single
// item. In order to ensure that we continue with the correct context the lookup
// sorts the items with the matching ID from new to old and return the top item.
func (c Conn) readFileInfo(info *core.Info) error {
	// Build the basic match aggregation.
	andMatch := []bson.M{{"docID": info.DocID}}

	// If a fileID is set lookup by fileID
	if info.FileID != "" {
		andMatch = append(andMatch, bson.M{"fileID": info.FileID})
	} else if info.Name != "" { // If a name is set lookup by name
		andMatch = append(andMatch, bson.M{"name": info.Name})
	}

	// Check if a directory is specified. If so add an additional filter to the
	// match aggregation to only return items for the specified dir.
	if info.Dir != "/" && info.Dir != "" {
		andMatch = append(andMatch, bson.M{
			"$or": []bson.M{
				{"dir": info.Dir},
				{"dir": bson.M{"$regex": fmt.Sprintf("%s.*", info.Dir)}},
			},
		})
	}

	agg := []bson.M{
		{"$match": bson.M{"$and": andMatch}},
		{"$sort": bson.M{"modified": -1}}, // Newest first
	}

	return c.fileInfo.Pipe(agg).AllowDiskUse().One(info)
}

// deleteFileInfo can be used to cleanup the defunct file descriptions that
// remain as a result of the update process.
func (c Conn) deleteFileInfo(info *core.Info) error {
	return c.fileInfo.RemoveId(info.ID)
}

// InfoList returns a list of info items as an io.Reader. This can be used to
// render info lists or build operational logic spanning multiple items.
func (c Conn) InfoList(info *core.Info, opts *core.RequestOptions) (io.Reader, error) {
	if c.storageType == core.DocStorage {
		return c.docInfoList(opts)
	}
	return c.fileInfoList(info, opts)
}

// docInfoList returns a list of info objects for the current document
// collection. Output can be modified through a core.RequestOptions object.
func (c Conn) docInfoList(opts *core.RequestOptions) (io.Reader, error) {
	var agg = []bson.M{{"$sort": bson.M{"modified": -1}}} // Newest first

	// If opts.Verbose then we want to include items that were deleted
	if !opts.Verbose {
		agg = append(agg, bson.M{
			"$match": bson.M{
				"deleted": bson.M{"$eq": nil},
			},
		})
	}

	// add the skip offset and remove the internal ID
	agg = append(agg, []bson.M{
		{"$skip": opts.Page[0]},
		{"$project": bson.M{
			"_id":      0,
			"checksum": 0,
			"size":     0,
			"dir":      0,
		}},
	}...)

	// The following fields are filtered from the response when the caller isn't
	// requesting verbose output.
	if !opts.Verbose {
		agg = append(agg, bson.M{"$project": bson.M{
			"created":       0,
			"modified":      0,
			"validators":    0,
			"version":       0,
			"latestRelease": 0,
			"owner":         0,
			"name":          0,
		}})
	}

	// If the size of the page is -1 we select everything.
	if opts.Page[1] != -1 {
		agg = append(agg, bson.M{"$limit": opts.Page[1]})
	}

	// Execute the db call and process the output using the pipeReader method
	return c.pipeReader(c.docInfo.Pipe(agg).AllowDiskUse(), opts)
}

// fileInfoList uses the info description to determine what storage scope you
// are in and returns the files connected to it.
func (c Conn) fileInfoList(info *core.Info, opts *core.RequestOptions) (io.Reader, error) {
	var agg []bson.M
	var andMatch = []bson.M{{"docID": info.DocID}}

	if info.Dir != "/" && info.Dir != "" {
		andMatch = append(andMatch, bson.M{
			"$or": []bson.M{
				{"dir": info.Dir},
				{"dir": bson.M{"$regex": fmt.Sprintf("%s.*", info.Dir)}},
			},
		})
	}

	// If opts.Verbose then we want to include items that were deleted
	if !opts.Verbose {
		andMatch = append(andMatch, bson.M{"deleted": bson.M{"$eq": nil}})
	}

	// Initial match aggregation.
	agg = []bson.M{{"$match": bson.M{"$and": andMatch}}}

	// add the skip offset and remove the internal ID
	agg = append(agg, []bson.M{
		{"$skip": opts.Page[0]},
		{"$project": bson.M{
			"_id": 0,
		}},
	}...)

	// The following fields are filtered from the response when the caller isn't
	// requesting verbose output.
	if !opts.Verbose {
		agg = append(agg, bson.M{"$project": bson.M{
			"docID":         0,
			"created":       0,
			"modified":      0,
			"validators":    0,
			"latestRelease": 0,
			"owner":         0,
		}})
	}

	// If the size of the page is -1 we select everything.
	if opts.Page[1] != -1 {
		agg = append(agg, bson.M{"$limit": opts.Page[1]})
	}

	// Execute the db call and process the output using the pipeReader method
	return c.pipeReader(c.fileInfo.Pipe(agg).AllowDiskUse(), opts)
}

// pipeReader formats the provided *mgo.Pipe based on the *core.RequestOptions.
func (c Conn) pipeReader(p *mgo.Pipe, opts *core.RequestOptions) (io.Reader, error) {
	var r io.Reader
	var err error

	// Pick the response format based on the Type in the request options.
	switch opts.Type {
	case core.FeedResponse:
		r = NewIterReader(p.Iter())
	default:
		var doc []interface{}
		var dat []byte
		if err = p.All(&doc); err == nil {
			dat, err = json.Marshal(doc)
			r = bytes.NewBuffer(dat)
		}
	}
	return r, err
}

// init is called on load and tries to register mongo as a driver with the core.
func init() {
	core.RegisterDriver(driverName, Driver{})
}
