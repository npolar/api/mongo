# Mongo [![Go Report Card](https://goreportcard.com/badge/gitlab.com/npolar/api/mongo)](https://goreportcard.com/report/gitlab.com/npolar/api/mongo)

Mongo is a reference storage implementation based on the
[core](https://gitlab.com/npolar/api/core) library and mongodb.

|Branch|Pipeline|Coverage|
|---|---|---|
|[**Master**](https://gitlab.com/npolar/api/mongo/tree/master)|[![pipeline status](https://gitlab.com/npolar/api/mongo/badges/master/pipeline.svg)](https://gitlab.com/npolar/api/mongo/commits/master)|[![coverage report](https://gitlab.com/npolar/api/mongo/badges/master/coverage.svg)](https://gitlab.com/npolar/api/mongo/commits/master)|
|[**Develop**](https://gitlab.com/npolar/api/mongo/tree/develop)|[![pipeline status](https://gitlab.com/npolar/api/mongo/badges/develop/pipeline.svg)](https://gitlab.com/npolar/api/mongo/commits/develop)|[![coverage report](https://gitlab.com/npolar/api/mongo/badges/develop/coverage.svg)](https://gitlab.com/npolar/api/mongo/commits/develop)|
