package mongo

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"testing"
	"time"

	"gitlab.com/npolar/api/core"
)

const (
	data = `{
		"_id":"plz-ignore",
		"en":"Hello world",
		"jp":"こんにちは世界"
	}`
	updateData = `{
		"en":"Hello world",
		"jp":"こんにちは世界",
		"nl":"Hallo wereld"
	}`
)

var (
	options = map[core.StorageType]struct {
		Prefix string
		Opts   []byte
	}{
		core.DocStorage: {
			"Doc",
			[]byte(`{
				"database":"api",
				"collection":"test",
				"username":"TestUser",
				"password":"GottaTestEmAll",
				"supportedTypes":["application/json"],
				"servers":["localhost:27017"]
			}`),
		},
		core.FileStorage: {
			"File",
			[]byte(`{
				"database":"api",
				"collection":"testFS",
				"username":"TestUser",
				"password":"GottaTestEmAll",
				"authDB":"api",
				"supportedTypes":[
					"application/json",
					"application/vnd+geojson"
				],
				"servers":["localhost:27017"]
			}`),
		},
	}
)

// Test core inteface compliance.
// @NOTE subtest ordering is somewhat significant. The reason for this is to
// prevent additional setup for each test. However this means you have to be
// careful when changing stuff around and updating variable states.
func TestInterface(t *testing.T) {
	for st, mode := range options {
		// Establish a storage connection
		c, err := core.NewConn(driverName, st, mode.Opts)
		if err != nil {
			t.Fatal(err)
		}

		// Initialize a metadata object
		var info *core.Info
		if st == core.DocStorage {
			info = core.NewDocumentInfo()
		} else {
			info = core.NewFileInfo("parent1")
			info.Dir = "/test/files"
		}
		info.Owner["noopUser"] = &core.AccessRights{}
		info.Owner[mode.Prefix] = &core.AccessRights{
			Create: true,
			Read:   true,
			Update: true,
			Delete: true,
		}
		info.Type = "application/json"
		info.Released = time.Now().UTC().Add(time.Second * 3600)

		// Create a new reader for our data
		dbuf := bytes.NewBuffer([]byte(data))

		// Setup a new cancelation context to flag when we are done reading.
		ctx, cancel := context.WithCancel(context.Background())

		// CREATE
		t.Run(
			fmt.Sprintf("%s:CREATE", mode.Prefix),
			func(t *testing.T) {
				info.FileID = ""
				err = c.Create(info, dbuf)
				if err != nil {
					t.Fatal(err)
				}
			},
		)

		// READ
		t.Run(
			fmt.Sprintf("%s:READ", mode.Prefix),
			func(t *testing.T) {
				dr, err := c.Read(info, ctx)
				if err != nil {
					t.Fatal(err)
				}
				dbuf.Reset() // Reset the buffer contents
				dbuf.ReadFrom(dr)
				if dbuf.Len() == 0 {
					t.Fatal("Zero bytes returned")
				}
				cancel() // Cancel the contex once we are done
			},
		)

		// UPDATE
		t.Run(
			fmt.Sprintf("%s:UPDATE", mode.Prefix),
			func(t *testing.T) {
				info.Owner = map[string]*core.AccessRights{"unknown": nil}
				err = c.Update(info, dbuf)
				if err == nil {
					t.Fatal("It should return an Unauthorized error")
				}
				info.Owner = map[string]*core.AccessRights{
					mode.Prefix: {
						Create: true,
						Read:   true,
						Update: true,
						Delete: true,
					},
				}
				dbuf.Reset() // Reset the buffer contents
				dbuf.WriteString(updateData)
				err = c.Update(info, dbuf)
				if err != nil {
					t.Fatal(err)
				}
			},
		)

		// Setup a new cancelation context to flag when we are done reading.
		ctx, cancel = context.WithCancel(context.Background())

		// READ UPDATE
		t.Run(
			fmt.Sprintf("%s:READ update", mode.Prefix),
			func(t *testing.T) {
				dr, err := c.Read(info, ctx)
				if err != nil {
					t.Fatal(err)
				}
				dbuf.Reset() // Reset the buffer contents
				dbuf.ReadFrom(dr)
				cancel() // Cancel the context
				var doc map[string]interface{}
				err = json.Unmarshal(dbuf.Bytes(), &doc)
				if err != nil {
					t.Fatal(err)
				}
				if _, ok := doc["nl"]; !ok {
					t.Fatal(`Expected "nl" key to be present`)
				}
			},
		)

		// DELETE
		t.Run(
			fmt.Sprintf("%s:DELETE", mode.Prefix),
			func(t *testing.T) {
				// Should not delete if not authorized
				info.Owner = map[string]*core.AccessRights{"unknown": nil}
				err = c.Delete(info)
				if err == nil {
					t.Fatal("It should return an Unauthorized error")
				}
				info.Owner = map[string]*core.AccessRights{
					mode.Prefix: {
						Create: true,
						Read:   true,
						Update: true,
						Delete: true,
					},
				}
				err = c.Delete(info)
				if err != nil {
					t.Fatal(err)
				}
			},
		)

		// Test READ errors
		t.Run(
			fmt.Sprintf("%s:READ errors", mode.Prefix),
			func(t *testing.T) {
				_, err := c.Read(info, nil)
				if err == nil {
					t.Fatal(
						"It should throw an error when reading a deleted item.",
					)
				}
				blank := &core.Info{}
				_, err = c.Read(blank, nil)
				if err == nil {
					t.Fatal("It should throw an error when no Info is found.")
				}
				copy := &core.Info{}
				copy.Deleted = time.Time{}
				copy.Released = time.Now().UTC().Add(time.Second * 5)
				copy.DocID = core.UUIDv4()
				copy.FileID = core.UUIDv4()
				copy.Owner = info.Owner
				c.WriteInfo(copy)
				copy.Owner = map[string]*core.AccessRights{"unknown": nil}
				_, err = c.Read(copy, nil)
				if err == nil {
					t.Fatal(`It should generate an authorization error`,
						`for unknown users`)
				}

				if st == core.FileStorage {
					copy.Owner = info.Owner
					_, err = c.Read(copy, nil)
					if err == nil {
						t.Fatal(`It should generate an error when reading a`,
							`file without context`)
					}
				}
			},
		)

		// UPDATE deleted
		t.Run(
			fmt.Sprintf("%s:UPDATE after delete", mode.Prefix),
			func(t *testing.T) {
				dbuf.Reset()
				dbuf.WriteString(updateData)
				err = c.Update(info, dbuf)
				if err != nil {
					t.Fatal(err)
				}
			},
		)

		// InloList retrieval for index purposes
		t.Run(
			fmt.Sprintf("%s:InfoListRequest", mode.Prefix),
			func(t *testing.T) {
				opts := core.DefaultRequestOptions()
				ir, err := c.InfoList(info, opts)
				if err != nil {
					t.Fatal(err)
				}
				dbuf.Reset()
				dbuf.ReadFrom(ir)
				if dbuf.Len() == 0 {
					t.Fatal("It should be larger than 0 bytes.")
				}
				if dbuf.String() == "null" {
					t.Fatal(`Response should not be "null"`)
				}
				// Test Feed response
				opts.Type = core.FeedResponse
				ir, err = c.InfoList(info, opts)
				if err != nil {
					t.Fatal(err)
				}
				var b = make([]byte, 1)
				_, err = ir.Read(b)
				if err != nil {
					t.Fatal(err)
				}
				dbuf.Reset()
				dbuf.Write(b)
				_, err = dbuf.ReadFrom(ir)
				if err != nil {
					t.Fatal(err)
				}
				if dbuf.Len() == 0 {
					t.Fatal("It should be larger than 0 bytes.")
				}
				if string(b) == "null" {
					t.Fatal(`Response should not be "null"`)
				}
				if err != nil {
					t.Fatal(err)
				}
			},
		)

		// Create using a WriteCloser
		t.Run(
			fmt.Sprintf("%s:WriteCloser", mode.Prefix),
			func(t *testing.T) {
				info.ID = ""
				info.DocID = core.UUIDv4()
				info.FileID = ""
				wc, err := c.NewWriteCloser(info)
				if err != nil {
					t.Fatal("Expected to open a io.WriteCloser but got:", err)
				}
				dbuf.Reset()
				dbuf.WriteString(data)
				_, err = io.Copy(wc, dbuf)
				if err != nil {
					t.Fatal("Expected io.Copy to work but got:", err)
				}
				err = wc.Close()
				if err != nil {
					t.Fatal("Expected write closer to close but got:", err)
				}
				_, err = c.NewWriteCloser(&core.Info{})
				if err == nil {
					t.Fatal("It should return a type error")
				}
			},
		)

		// Reject unsupported types
		t.Run(
			fmt.Sprintf("%s:Reject unsupported", mode.Prefix),
			func(t *testing.T) {
				info.Type = "application/not+supported"
				err = c.Create(info, dbuf)
				if err == nil {
					t.Fatal(
						"Create should return an error for unsupported types",
					)
				}
				err = c.Update(info, dbuf)
				if err == nil {
					t.Fatal(
						"Update should return an error for unsupported types",
					)
				}
			},
		)

		// List supported types
		t.Run(
			fmt.Sprintf("%s:List supported formats", mode.Prefix),
			func(t *testing.T) {
				var set map[string][]interface{}
				json.Unmarshal(mode.Opts, &set)
				for i, f := range set["supportedTypes"] {
					if c.Formats()[i] != f.(string) {
						t.Fatal(
							"Expected Formats to list same items as configured",
						)
					}
				}
			},
		)

		// Write info object to storage
		t.Run(
			fmt.Sprintf("%s:WriteInfo", mode.Prefix),
			func(t *testing.T) {
				if st == core.DocStorage {
					info = core.NewDocumentInfo()
				} else {
					info = core.NewFileInfo("parent2")
				}
				err = c.WriteInfo(info)
				if err != nil {
					t.Fatal("It should write info without errors ->", err)
				}
				err = c.WriteInfo(&core.Info{})
				if err == nil {
					t.Fatal("It should return an error on a blank docID.")
				}
				if st == core.FileStorage {
					err = c.WriteInfo(&core.Info{DocID: core.UUIDv4()})
					if err == nil {
						t.Fatal("It should return an error on a blank FileID.")
					}
				}
			},
		)

		// Retrieve info object from storage
		t.Run(
			fmt.Sprintf("%s:ReadInfo", mode.Prefix),
			func(t *testing.T) {
				// fetch the info we wrote before
				ri := &core.Info{
					DocID:  info.DocID,
					FileID: info.FileID,
				}
				err = c.ReadInfo(ri)
				if err != nil {
					t.Fatal("It should read info without errors ->", err)
				}

				// Encode and Decode into a map in order to do a key, value
				// comparison of the Info objects.
				var rm, im map[string]interface{}
				rb, err := ri.Json()
				if err != nil {
					t.Fatal("Failed to encode to json")
				}
				ib, err := info.Json()
				if err != nil {
					t.Fatal("Failed to encode to json")
				}

				json.Unmarshal(rb, &rm)
				json.Unmarshal(ib, &im)
				for k, v := range im {
					if rm[k] != v {
						t.Errorf(
							`It should have the same metadata but got mismatch
							for Key:%s -> a=%v, b=%v`, k, rm[k], v,
						)
					}
				}
			},
		)

		t.Run(
			fmt.Sprintf("%s:Close Conn", mode.Prefix),
			func(t *testing.T) {
				time.Sleep(time.Millisecond * 500)
				err = c.Close()
				if err != nil {
					t.Fatal(err)
				}
			},
		)
	}
}

func TestWildCardTypeSupport(t *testing.T) {
	c, err := core.NewConn("mongo", core.FileStorage,
		[]byte(`{
				"database":"api",
				"collection":"testFS",
				"username":"TestUser",
				"password":"GottaTestEmAll",
				"supportedTypes":["*/*"],
				"servers":["localhost:27017"]
			}`),
	)

	if err != nil {
		t.Fatal(err)
	}

	var dbuf = bytes.NewBuffer([]byte(data))
	var types = []string{
		"application/json",
		"image/jpeg",
		"application/octet-stream",
	}

	for _, mt := range types {
		// CREATE with different types
		info := core.NewFileInfo("abc")
		info.Type = mt
		info.Owner["anyType"] = &core.AccessRights{
			Create: true,
			Read:   true,
			Update: true,
			Delete: true,
		}
		t.Run(
			fmt.Sprintf("CREATE:%s", mt),
			func(t *testing.T) {
				err = c.Create(info, dbuf)
				if err != nil {
					t.Fatal(err)
				}
			},
		)
	}

}

func TestExceptions(t *testing.T) {
	for st, mode := range options {
		// CONN ERRORS
		t.Run(
			fmt.Sprintf("%s:Check Conn Errors", mode.Prefix),
			func(t *testing.T) {
				_, err := core.NewConn(driverName, st, nil)
				if err == nil {
					t.Fatal("It should throw an error when not configured.")
				}
				_, err = core.NewConn(driverName, st, []byte(`{}`))
				if err == nil {
					t.Fatal("It should throw an error when login fails.")
				}
				_, err = core.NewConn(driverName, st, []byte(`{
					"database":"api",
					"username":"TestUser",
					"password":"GottaTestEmAll"
				}`))
				if err == nil {
					t.Fatal("It should throw an error on no collection.")
				}
			},
		)
	}
}
